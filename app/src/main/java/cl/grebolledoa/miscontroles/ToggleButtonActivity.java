package cl.grebolledoa.miscontroles;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import android.widget.ToggleButton;

public class ToggleButtonActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toggle_button);
    }

    public void onClicToggleButton(View v){
        ToggleButton tb = (ToggleButton) v;

        if(tb.isChecked()){
            Toast.makeText(this, "La maquina ahora esta activa", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "La maquina ahora esta inactiva", Toast.LENGTH_SHORT).show();
        }
    }
}