package cl.grebolledoa.miscontroles;

import androidx.appcompat.app.AppCompatActivity;

import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickBtnHomero(View v){
        ImageButton btn = (ImageButton) v;

        btn.setImageDrawable(getResources().getDrawable(R.drawable.homero2));


        Toast.makeText(this, "Click en homero", Toast.LENGTH_SHORT).show();
    }
}