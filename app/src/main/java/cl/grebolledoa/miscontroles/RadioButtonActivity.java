package cl.grebolledoa.miscontroles;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class RadioButtonActivity extends AppCompatActivity {

    private RadioGroup rgLenguaje;
    private RadioGroup rgSexo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio_button);
        this.rgLenguaje = findViewById(R.id.rg_lenguaje);
        this.rgSexo = findViewById(R.id.rg_sexo);
    }

    public void onClicBtn(View v){
        //Acá obtenemos el id del radio button que se eligio
        int idLenguajeSelected = this.rgLenguaje.getCheckedRadioButtonId();

        if(idLenguajeSelected != -1){
            //obtener el widget por medio del id
            RadioButton rbLenguajeSelected = findViewById(idLenguajeSelected);

            int idSexoSelected = this.rgSexo.getCheckedRadioButtonId();

            if(idSexoSelected != -1){

                RadioButton rbSexoSelected = findViewById(idSexoSelected);

                Toast.makeText(this, "Tu sexo es: " + rbSexoSelected.getText().toString() +
                                " y tu lenguaje favorito es: "+ rbLenguajeSelected.getText().toString(),
                        Toast.LENGTH_SHORT).show();

                this.rgLenguaje.clearCheck();
                this.rgSexo.clearCheck();
            }else{
                Toast.makeText(this, "Favor seleccionar el sexo",
                        Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(this, "Favor seleccionar el lenguaje",
                    Toast.LENGTH_SHORT).show();
        }
    }
}