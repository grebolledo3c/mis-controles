package cl.grebolledoa.miscontroles;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TimePicker;
import android.widget.Toast;

public class RelojActivity extends AppCompatActivity {

    private TimePicker tpReloj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reloj);

        this.tpReloj = findViewById(R.id.tp_reloj);
    }

    public void onClicBtAgendarHora(View v){
        int hora = this.tpReloj.getCurrentHour();
        int minutos = this.tpReloj.getCurrentMinute();
        String ampm = "AM";

        if(hora > 12){
            hora -= 12;
            ampm = "PM";
        }

        if(hora == 0){
            ampm = "AM";
        }

        Toast.makeText(this, "Se agendo una hora para hoy a las: "
                + hora + ":" + minutos + " " + ampm, Toast.LENGTH_SHORT).show();
    }
}