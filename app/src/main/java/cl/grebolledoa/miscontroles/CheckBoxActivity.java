package cl.grebolledoa.miscontroles;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

public class CheckBoxActivity extends AppCompatActivity {

    private CheckBox cbTyc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_box);

        this.cbTyc = findViewById(R.id.cb_tyc);
    }

    public void onClicBtn(View v){
        if(this.cbTyc.isChecked()){
            Toast.makeText(this, "Acepto terminos y condiciones", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Aún no acepta los terminos y condiciones", Toast.LENGTH_SHORT).show();
        }
    }
}